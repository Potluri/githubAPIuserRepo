//
//  Cell.swift
//  githubAPIUserRepo
//
//  Created by Potluri on 7/11/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit

class Cell: UICollectionViewCell {
    var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.red
        label.text = "Bob Lee"
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
//        label.minimumScaleFactor = 0.5
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var createdTimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.blue
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var licenseLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
        renderCell()
    }
    
    func renderCell(){
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 2.0
    }
    
    func addViews(){
        addSubview(nameLabel)
        addSubview(descriptionLabel)
        addSubview(createdTimeLabel)
        addSubview(licenseLabel)
        
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: nameLabel)
        addConstraintsWithFormat("H:|-8-[v0]-8-[v1(50)]|", views: createdTimeLabel,licenseLabel)
        addConstraintsWithFormat("H:|-8-[v0]-8-|", views: descriptionLabel)
        addConstraintsWithFormat("V:|-8-[v0(44)]-4-[v1(44)]-4-[v2]-8-|", views: nameLabel,createdTimeLabel,descriptionLabel)
        addConstraintsWithFormat("V:|-8-[v0(44)]-4-[v1(44)]-4-[v2]-8-|", views: nameLabel,licenseLabel,descriptionLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
