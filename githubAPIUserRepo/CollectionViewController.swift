//
//  CollectionViewController.swift
//  githubAPIUserRepo
//
//  Created by Potluri on 7/11/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import UIKit


class CollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {let cellId = "cellId"
    
    var page = 1
    var segmentedValue = 0
    var responses: [[ReceivedResponse]]?
    
    
    let userTextField: UITextField = {
        let tf = UITextField()
        tf.textAlignment = .center
        tf.attributedPlaceholder = NSAttributedString(string: "USER")
        tf.autocorrectionType = .no
        tf.borderStyle = .roundedRect
        tf.autocapitalizationType = .none
        tf.returnKeyType = .done
        tf.becomeFirstResponder()
        return tf
    }()
    
    let segmentedControl: UISegmentedControl = {
        var sc = UISegmentedControl(items: ["List","Grid"])
        sc.selectedSegmentIndex = 0
        sc.tintColor = UIColor.blue
        return sc
    }()
    
    
    lazy var collectionView: UICollectionView = {
        let layOut = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layOut)
        cv.alwaysBounceVertical = true
        cv.isHidden = false
        layOut.scrollDirection = .vertical
        layOut.minimumLineSpacing = 1
        cv.backgroundColor = UIColor.white
        cv.isPagingEnabled = false
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.green
        collectionView.register(Cell.self, forCellWithReuseIdentifier: cellId)
        userTextField.delegate = self
        segmentedControl.addTarget(self, action: #selector(segmentedValueChanged), for: .valueChanged)
        view.addSubview(userTextField)
        view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: userTextField)
        view.addConstraintsWithFormat("V:|-100-[v0(50)]", views: userTextField)
    }
    
    
    
    func addSubViews(){
        userTextField.removeFromSuperview()
        view.addSubview(userTextField)
        view.addSubview(segmentedControl)
        view.addSubview(collectionView)

        view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: userTextField)
        view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: segmentedControl)
        view.addConstraintsWithFormat("H:|-5-[v0]-5-|", views: collectionView)
        view.addConstraintsWithFormat("V:|-20-[v0(40)]-10-[v1(40)]-10-[v2]|", views: userTextField,segmentedControl,collectionView)
    }
    
    
    @objc func segmentedValueChanged(_ sender: UISegmentedControl!){
        segmentedValue = (sender.selectedSegmentIndex)
        if AuthService.authservice.responses != nil {
            self.responses = self.segmentedValue == 0 ? AuthService.authservice.responses!.chunked(by: 1) : AuthService.authservice.responses!.chunked(by: 2)
        }
        collectionView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userTextField.resignFirstResponder()
        AuthService.authservice.getData(userLabelText: userTextField.text!, page: page) { (success) in
            if success {
                print("???>><<>?>><>?><??::********Success*************??????////////////////")
                self.segmentedValueChanged(self.segmentedControl)
            } else {
                print("???>><<>?>><>?><??::********failed*************??????////////////////")
            }
        }
        addSubViews()
        return true
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return responses?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return segmentedValue == 0 ? 1 : 2
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 30 {
            self.getNewData(page: page + 1)
        }
    }
    
    func getNewData(page: Int){
        AuthService.authservice.getData(userLabelText: userTextField.text!, page: page) { (success) in
            if success {
                print("???>><<>?>><>?><??::********Success*************??????////////////////\(self.page)")
                self.segmentedValueChanged(self.segmentedControl)
            } else {
                print("???>><<>?>><>?><??::********failed*************??????////////////////\(self.page)")
            }
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! Cell
        guard let responses = responses else { return UICollectionViewCell() }
        cell.nameLabel.text = responses[indexPath.section][indexPath.row].name!
        cell.createdTimeLabel.text = getFormattedDate(dateString: responses[indexPath.section][indexPath.row].dateCreated!)
        cell.licenseLabel.text = "Other"
        cell.descriptionLabel.text = responses[indexPath.section][indexPath.row].description!
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return segmentedValue == 0 ? CGSize(width: collectionView.frame.width - 40, height: 148) : CGSize(width: (collectionView.frame.width - 10)/2, height: 200)
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()

        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .centeredHorizontally, animated: true)
        }
        collectionView.reloadData()
    }
    
    func getFormattedDate(dateString:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = "MMM-dd, yyyy"
        return dateObj == nil ? "" : dateFormatter.string(from: dateObj!)
    }
    
}
