//
//  AuthService.swift
//  githubAPIUserRepo
//
//  Created by Potluri on 7/12/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation


class AuthService {
    static let authservice = AuthService()
    
    var responses:[ReceivedResponse]? = [ReceivedResponse]()
    
    let defaults = UserDefaults.standard
    
    func getData(userLabelText: String, page: Int, completion: @escaping (_ success: Bool)-> ()){
        let txt = "https://api.github.com/users/" + (userLabelText) + "/repos?page=" + String(page) + "&per_page=10"
        let url = URL(string: txt)!
        print(url)
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            guard let data = data else {
                print("Data is not received")
                return
            }
            DispatchQueue.main.async {
                print(data)
            }
            do {
                let jsonData = try JSONDecoder().decode([ReceivedResponse]?.self, from: data)
                
                DispatchQueue.main.async {
                    if let json = jsonData {
                        self.responses?.append(contentsOf: json)
                    }
                    completion(true)
                }
            }catch let jsonError {
                print(jsonError)
            }
        }.resume()
    }
}
