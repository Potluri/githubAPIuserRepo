//
//  Model.swift
//  githubAPIUserRepo
//
//  Created by Potluri on 7/12/18.
//  Copyright © 2018 Potluri. All rights reserved.
//

import Foundation

struct Dict : Decodable {
    var key: String?
    var name: String?
    
    enum CodingKeys: String, CodingKey{
        case key
        case name
    }
    
    init(from decoder: Decoder) throws {
        let valueArray = try decoder.container(keyedBy: CodingKeys.self)
        self.key = try valueArray.decode(String.self,forKey: CodingKeys.key)
        self.name = try valueArray.decode(String.self, forKey: CodingKeys.name)
    }
    
    init(key: String, name: String){
        self.key = key
        self.name = name
    }
}
struct ReceivedResponse: Decodable {
    var name: String?
    var license: Dict?
    var dateCreated: String?
    var description: String?
    
    enum CodingKeys: String, CodingKey{
        case name
        case description
        case dateCreated = "created_at"
        case license
    }
    
    init(from decoder: Decoder) throws {
        let valueArray = try decoder.container(keyedBy: CodingKeys.self)
        self.dateCreated = try valueArray.decode(String.self,forKey: CodingKeys.dateCreated)
        do {
        self.description = try valueArray.decode(String.self, forKey: CodingKeys.description)
        } catch {
            self.description = "No description Found"
        }
        do {
        self.license = try valueArray.decode(Dict.self, forKey: CodingKeys.license)
        } catch {
            self.license = Dict(key: "none found", name: "none found")
        }
        self.name = try valueArray.decode(String.self, forKey: CodingKeys.name)
    }
}
